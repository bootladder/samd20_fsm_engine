#include "SAMD20_FSM_Engine.h"
#include "UART.h"
//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "samd20.h"
#include "hal_gpio.h"

//-----------------------------------------------------------------------------
#define PERIOD_FAST     100
#define PERIOD_SLOW     500

HAL_GPIO_PIN(LED,      A, 14)
HAL_GPIO_PIN(BUTTON,   A, 15)

static void (*tickhandler)(void);
static uint16_t _periodmillis = 10;

//-----------------------------------------------------------------------------
static void timer_set_period_millis(uint16_t millis)
{
  //the frequency is 8KHz.
  //compare value should be millis*8
  TC0->COUNT16.CC[0].reg = millis*8; 
  TC0->COUNT16.COUNT.reg = 0;
}


//-----------------------------------------------------------------------------
void irq_handler_tc0(void)
{
  if (TC0->COUNT16.INTFLAG.reg & TC_INTFLAG_MC(1))
  {
    HAL_GPIO_LED_toggle();
    if( tickhandler )
      tickhandler();

    TC0->COUNT16.INTFLAG.reg = TC_INTFLAG_MC(1);
  }
}

//-----------------------------------------------------------------------------
static void timer_init(void)
{
  PM->APBCMASK.reg |= PM_APBCMASK_TC0;

//Configure TC0 to use GCLK3 8MHz
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(TC0_GCLK_ID) |
      GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN(3);

  //Prescale by 1024 to get 8KHz 
  TC0->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_WAVEGEN_MFRQ |
      TC_CTRLA_PRESCALER_DIV1024 | TC_CTRLA_PRESCSYNC_RESYNC;

  TC0->COUNT16.COUNT.reg = 0;

  timer_set_period_millis(PERIOD_SLOW);

  TC0->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;

  TC0->COUNT16.INTENSET.reg = TC_INTENSET_MC(1);
  NVIC_EnableIRQ(TC0_IRQn);
}

void FSMEngine_SetTickHandler(void (*handler)(void))
{
    tickhandler = handler;
} 
void FSMEngine_SetTickPeriodMillis(uint16_t millis)
{
    _periodmillis = millis; 
    timer_set_period_millis(millis);
  
}
void FSMEngine_Init(void)
{
  asm volatile ("cpsie i");

  //Clocks
  


  timer_init();
  uart_init(115200);
  //uart_puts("UART INIT");


  HAL_GPIO_LED_out();
  HAL_GPIO_LED_clr();

  HAL_GPIO_BUTTON_in();
  HAL_GPIO_BUTTON_pullup();
}
