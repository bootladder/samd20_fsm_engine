##############################################################################
BUILD = build
PUBLIC= public 
BIN = libSAMD20_FSM_Engine
##############################################################################
.PHONY: all directory clean size

CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size

CFLAGS += -W -Wall --std=gnu99 -O0
CFLAGS += -fdata-sections -ffunction-sections
CFLAGS += -funsigned-char -funsigned-bitfields
CFLAGS += -mcpu=cortex-m0plus -mthumb
CFLAGS += -MD -MP -MT $(BUILD)/$(*F).o -MF $(BUILD)/$(@F).d

LDFLAGS += -mcpu=cortex-m0plus -mthumb

INCLUDES += \
  -I../include \
  -I../samd20_cmsis_headers \
  -I../arm_cmsis_headers

DEFINES += \
  -D__SAMD20J18__ \
  -DDONT_USE_CMSIS_INIT \
  -DF_CPU=8000000 

CFLAGS += $(INCLUDES) $(DEFINES)
OBJS = build/SAMD20_FSM_Engine.o build/UART.o build/StateMachine.o


all: directory libSAMD20_FSM_Engine.a size

libSAMD20_FSM_Engine.a: $(OBJS)
	@echo $(AR) -r -o $@ $(OBJS) 
	@$(AR) -r -o public/libSAMD20_FSM_Engine.a $(OBJS) 
	@cp SAMD20_FSM_Engine.h public/
	@cp StateMachine.h public/

build/SAMD20_FSM_Engine.o: 
	@echo $(CC) $(CFLAGS) SAMD20_FSM_Engine.c -c -o $@
	@$(CC) $(CFLAGS) SAMD20_FSM_Engine.c -c -o $@

build/UART.o: 
	@echo $(CC) $(CFLAGS) UART.c -c -o $@
	@$(CC) $(CFLAGS) UART.c -c -o $@

build/StateMachine.o:
	@echo $(CC) $(CFLAGS) StateMachine.c -c -o $@
	@$(CC) $(CFLAGS) StateMachine.c -c -o $@

directory:
	@mkdir -p $(BUILD)
	@mkdir -p $(PUBLIC)

clean:
	@echo clean
	@-rm -rf $(BUILD)
	@-rm -rf $(PUBLIC)

-include $(wildcard $(BUILD)/*.d)
