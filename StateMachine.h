/*
 * StateMachine.h
 *
 * Created: 9/12/2016 8:39:46 PM
 *  Author: bob
 */ 


#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_

#include <stdint.h>
#include <stdbool.h>

typedef bool (*Condition_Function_Ptr)(void * object_reference);
typedef void (*Do_Once_Function_Ptr)(void * object_reference);
#define MACHINE_D_NOTHING 255
#define MACHINE_C_ALWAYS 255

typedef struct {
	uint32_t condition_index;
	uint32_t nextState;	
} StateMachine_Condition_t;

typedef struct state_t{

	uint32_t state;
	uint32_t Do_Onces[32];
	StateMachine_Condition_t Conditions[32];
} StateMachine_State_t;

typedef struct machine_t{
	
	uint32_t curr_state;
	void * object_reference;
	Do_Once_Function_Ptr   * Do_Once_Funcs;
	Condition_Function_Ptr * Condition_Funcs;
	StateMachine_State_t   * Transition_Table;
} StateMachine_t;

void StateMachine_Run(StateMachine_t * machine);


#endif /* STATEMACHINE_H_ */