#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../API/StateMachine.h"

enum valve_states{
	
	VALVE_STATE_INIT,
	VALVE_STATE_CHECK_CONNECTED_VALVES,
	VALVE_STATE_WAIT_CHECK_TIMER,
	VALVE_STATE_DEBOUNCE1,
	VALVE_STATE_NOTCONNECTED,
	VALVE_STATE_IDLE,
	VALVE_STATE_DISCONNECTED,
	VALVE_STATE_MOVING,
	VALVE_STATE_FAILED,
	VALVE_STATE_DEBOUNCE2,
	VALVE_STATE_WAIT_BEEP,
	VALVE_STATE_LASTNUMBER,
};

StateMachine_t ValveStateMachine;
StateMachine_State_t ValveStates[] = {
	
	{
		.state = VALVE_STATE_CHECK_CONNECTED_VALVES,
		.Do_Onces = {
			Valve_Enable,
			Valve_Close,
		},
		.Conditions = {
			StateMachine_Always,VALVE_STATE_WAIT_CHECK_TIMER
		}
	},
	{
		.state = VALVE_STATE_WAIT_CHECK_TIMER,
		.Do_Onces = {
			StateMachine_Nothing
		},
		.Conditions = {
			
			{Valve_Closed,VALVE_STATE_DEBOUNCE1},
			{Valve_Timeout,VALVE_STATE_NOTCONNECTED}
			
		}
	}
	
};

char * Get_New_DoOnce_Name_String(uint32_t state)
{
	return 0;
}
char * Get_New_State_Name_String(Function_Ptr fptr)
{
	//uint8_t size = strlen();
	return 0;
}

void StateMachine_Print(StateMachine_t * machine)
{
	static char * statename;
	static char * do_oncenames[32];
	static char * condnames[32];
	
	uint8_t s_trav = 0, d_trav = 0, c_trav = 0;
	while( & (machine->states[s_trav]) != 0)
	{
		 statename = Get_New_State_Name_String(machine->states[s_trav].state);
		 
		 while( machine->states[s_trav].Do_Onces[d_trav] != 0 )
		 {
			 do_oncenames[d_trav] = Get_New_DoOnce_Name_String((Function_Ptr)0);
		 }
	}
}

int main(void)
{
  StateMachine_Print(&ValveStateMachine);

}
