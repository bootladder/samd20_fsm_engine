/*
 * StateMachine.c
 *
 * Created: 9/12/2016 8:39:33 PM
 *  Author: bob
 */ 
#include "StateMachine.h"
#include "UART.h"

void ASSERT_NOW(bool exp)
{
	if( !exp)
	{
		while(1){;}
	}
}

void StateMachine_Run(StateMachine_t * machine)
{
  //TEST
  static char a = 0;
  uart_putc(a++);

  if( ! machine )
    return;

	//Call all condition functions.
	uint32_t curr = machine->curr_state;
	StateMachine_Condition_t * cond = machine->Transition_Table[curr].Conditions;
	uint8_t c_trav = 0, d_trav = 0; //start at index 1.
									//zero is the placeholder for the start
	bool transition_time = false;
	while( cond[c_trav].condition_index != 0 )
	{
		//Call the condition function.
		//If any are met, break immediately and switch state
		uint8_t condition_index = cond[c_trav].condition_index ;
		
		if( condition_index == MACHINE_C_ALWAYS )
		{ 
			transition_time = true;
			break;
		}
		ASSERT_NOW(machine->Condition_Funcs[condition_index] != 0);
		if( machine->Condition_Funcs[condition_index](machine->object_reference) )
		{
			transition_time = true;
			break;
		}
		c_trav++;
	}
	
	if( transition_time )
	{
		//Change the current state
		machine->curr_state = cond[c_trav].nextState;
		curr = machine->curr_state;
		//call next state's Do_Once()'s
		if( machine->Transition_Table[curr].Do_Onces[0] == MACHINE_D_NOTHING )
			return;
			
		uint8_t do_once_index =	machine->Transition_Table[curr].Do_Onces[d_trav];
		do 
		{
			ASSERT_NOW(machine->Do_Once_Funcs[do_once_index] != 0);
			machine->Do_Once_Funcs[do_once_index](machine->object_reference);
			d_trav++;
			do_once_index =	machine->Transition_Table[curr].Do_Onces[d_trav];
		}
		while(do_once_index != 0);
		
	}
}


