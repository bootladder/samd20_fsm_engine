
#include <stdint.h>

void FSMEngine_Init(void);
void uart_puts(char *s);
void FSMEngine_SetTickPeriodMillis(uint16_t millis);
void FSMEngine_SetTickHandler(void (*handler)(void));
