#include "UART.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "samd20.h"
#include "hal_gpio.h"

HAL_GPIO_PIN(UART_TX,  B, 8)
HAL_GPIO_PIN(UART_RX,  B, 9)

//-----------------------------------------------------------------------------
void uart_init(uint32_t baud)
{
  //uint64_t br = (uint64_t)65536 * (F_CPU - 16 * baud) / F_CPU;
  uint64_t br = (uint64_t)65536 * (8000000 - 16 * baud) / 8000000;

  HAL_GPIO_UART_TX_out();
  HAL_GPIO_UART_TX_pmuxen(PORT_PMUX_PMUXE_D_Val);
  HAL_GPIO_UART_RX_in();
  HAL_GPIO_UART_RX_pmuxen(PORT_PMUX_PMUXE_D_Val);

  PM->APBCMASK.reg |= PM_APBCMASK_SERCOM4;

  //GCLK peripheral, generator 3
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(SERCOM4_GCLK_ID_CORE) |
      GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN(3);

  SERCOM4->USART.CTRLA.reg =
      SERCOM_USART_CTRLA_DORD | SERCOM_USART_CTRLA_MODE_USART_INT_CLK |
      //SERCOM_USART_CTRLA_RXPO(3/*PAD3*/) | SERCOM_USART_CTRLA_TXPO/*PAD2*/;
      SERCOM_USART_CTRLA_RXPO(1/*PAD1*/) /* |SERCOM_USART_CTRLA_TXPO */ /*PAD0*/;

  SERCOM4->USART.CTRLB.reg = SERCOM_USART_CTRLB_RXEN | SERCOM_USART_CTRLB_TXEN |
      SERCOM_USART_CTRLB_CHSIZE(0/*8 bits*/);

  SERCOM4->USART.BAUD.reg = (uint16_t)br;

  SERCOM4->USART.CTRLA.reg |= SERCOM_USART_CTRLA_ENABLE;

  uart_putc('z');
}

//-----------------------------------------------------------------------------
void uart_putc(char c)
{
  while (!(SERCOM4->USART.INTFLAG.reg & SERCOM_USART_INTFLAG_DRE));
  SERCOM4->USART.DATA.reg = c;
}

//-----------------------------------------------------------------------------
void uart_puts(char *s)
{
  while (*s)
    uart_putc(*s++);
}
